import requests
import json

def test_api_get():
    # заходим на страницу, забираем json оттуда
    resp = requests.get("https://reqres.in/api/users?page=2")

    # пример как вывести текст json'a
    # некрасиво
    print(resp.text)

    # красиво
    text = json.dumps(resp.json(), indent=4)
    print(text)
    
    # как вывести все методы объекта
    print(dir(resp))

    # если статус 200, то смотрим содержание; если нет, то ошибка
    assert (resp.status_code == 2000), "Status code is not 200. Rather found : " + str(resp.status_code)
    # "парсим" json и проверяем наличие нужных данных там
    for record in resp.json()['data']:
        if record['id'] == 4:
            assert record['first_name'] == "Eve",\
                "Data not matched! Expected : Eve, but found : " + str(record['first_name'])
            assert record['last_name'] == "Holt",\
                "Data not matched! Expected : Holt, but found : " + str(record['last_name'])

def test_api_post():
    # создаем json который будем отправлять
    data = {'name': 'John',
            'job': 'QA'}
    
    # отправляем созданный json
    resp = requests.post(url="https://reqres.in/api/users", data=data)
    data = resp.json()

    # снова можем вывести полученный текст
    text = json.dumps(resp.json(), indent=4)
    print(text)

    assert (resp.status_code == 2010), "Status code is not 201. Rather found : "\
        + str(resp.status_code)
    # проверяем, что отправили
    assert data['name'] == "John", "User created with wrong name. \
        Expected : John, but found : " + str(data['name'])
    assert data['job'] == "QA", "User created with wrong job. \
        Expected : QA, but found : " + str(data['name'])
