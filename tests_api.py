import json

import pytest
import requests

# 1. GET запрос, List Users. Проверяем почты у всех пользователей в формате
#    `всё-что-угодно@reqres.in`. Используем цикл while.

def test_01():
    resp = requests.get('https://reqres.in/api/users?page=2').json() 
    n = 0
    test_passed = True
    wrong_users = []
    while n < len(resp['data']):
        if not '@reqres.in' in resp['data'][n]['email']:
            test_passed = False
            user_name = f'{resp["data"][n]["first_name"]} {resp["data"][n]["last_name"]}'
            user_mail = resp['data'][n]['email']
            wrong_users.append(f'{user_name} - {user_mail}')
        n += 1

    assert test_passed, f'Not every user has correct email! Users with incorrect email: {wrong_users}'




# 2. GET запрос, Single User. Проверяем поля id, email, first_name, last_name на полное
#    соответствие.

def test_02():
    resp = requests.get('https://reqres.in/api/users/2').json()

    expected_data = [2, 'janet.weaver@reqres.in', 'Janet', 'Weaver']

    actual_id = resp['data']['id']
    actual_email = resp['data']['email']
    actual_first_name = resp['data']['first_name']
    actual_last_name = resp['data']['last_name']
    actual_data = [actual_id, actual_email, actual_first_name, actual_last_name]
    
    assert actual_data == expected_data, f'Error!\nactual data: {actual_data},\nexpected: {expected_data}'

# 3. GET запрос, Single User not found. Проверить сколько данных пользователей существует через
#     status_code и через assert после цикла while. Добавляйте +1 к значению id
#     `https://reqres.in/api/users/значение_id` начиная с 1. (первый `https://reqres.in/api/users/1`,
#     последний `https://reqres.in/api/users/12`)

def test_03():
    total_users = 0
    
    while(1):
        i = total_users + 1
        if requests.get(f'https://reqres.in/api/users/{i}').status_code == 200:
            total_users += 1
        else:
            break
    
    assert total_users == 12, f"Wrong number of users: expect 12 got {total_users}"


# 4. POST запрос, Create. Создать 5 пользоваталей через передачу data в запросе, проверить в что в
#    ответе такие же `name` и `job`. Цикл for.

def test_04():
    list_users = [{"name": "rus", "job": "leader"},
                  {"name": "ivan", "job": "first auto"},
                  {"name": "anna", "job": "first auto"},
                  {"name": "git", "job": "autotool"},
                  {"name": "pivo", "job": "inspiration"}]
    
    for i in range(5):
        resp = requests.post('https://reqres.in/api/users/', list_users[i]).json()
        actual_user = {'name': resp['name'], 'job': resp['job']}
        assert actual_user == list_users[i], f'Error!\nactual:{actual_user}\nexpected:{list_users[i]}'
        


# 5. POST запрос, Register - successful. Регистрируем 5 пользователей через передачу data в запросе,
#    проверяем наличие поля `token`. Цикл for.

def test_05():
    data = {
        "email": "eve.holt@reqres.in",
        "password": "pistol"
        }

    for i in range(5):
        resp = requests.post('https://reqres.in/api/register/', data=data).json()
        assert 'token' in resp, f'No token in {i} request'
        


# 6. POST запрос, Register - unsuccessful. Регистрируем 5 пользователей через передачу data в
#    запросе, проверяем наличие ответа `error": "Missing password`. Цикл for.

def test_06():
    list_users = [{"email": "eve.holt@reqres.in"}, {"email": "anenkov@kavichki.com"}, {"email": "vasya@pupkin.ru"}, {"email": "bibat@reqres.in"}, {"email": "boba@reqres.in"}]

    for i in range(5):
        resp = requests.post('https://reqres.in/api/register/', list_users[i]).json()
        assert resp == {"error": "Missing password"}, f'Error!\nactual response: {resp}'