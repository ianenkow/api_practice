### Зависимости
```
pytest
json
requests
```

### Запуск
```
Для запуска просто напишите pytest
```

### Тесты
Будем тестировать `https://reqres.in/`

1. GET запрос, List Users. Проверяем почты у всех пользователей в формате `всё-что-угодно@reqres.in`. Испульзуем цикл while.
2. GET запрос, Single User. Проверяем поля id, email, first_name, last_name на полное соответствие.
3. GET запрос, Single User not found. Проверить сколько данных пользователей существует через status_code и через assert после цикла while. Добавляйте +1 к значению id `https://reqres.in/api/users/значение_id` начиная с 1. (первый `https://reqres.in/api/users/1`, последний `https://reqres.in/api/users/12`)
4. POST запрос, Create. Создать 5 пользоваталей через передачу data в запросе, проверить в что в ответе такие же `name` и `job`. Цикл for.
5. POST запрос, Register - successful. Регистрируем 5 пользователей через передачу data в запросе, проверяем наличие поля `token`. Цикл for.
6. POST запрос, Register - unsuccessful. Регистрируем 5 пользователей через передачу data в запросе, проверяем наличие ответа `error": "Missing password`. Цикл for.
